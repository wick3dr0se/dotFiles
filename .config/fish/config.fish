# Start X at login
if status is-login
	if test -z "$DISPLAY" -a "$XDG_VTNR"
		exec startx -- -keeptty
	end
end

alias c "yay -Sc --noconfirm --color=auto"
alias del "rm -Rf"
alias dev "yay -S community/nodejs ; yarn dev"
alias rmv "yay -Runss --color=auto"
alias add "yay -S --color=auto"
alias list "yay -Qe --color=auto | less"
alias find "yay -Qe --color=auto | grep"
alias look "yay -Ss --color=auto"
