#!/usr/bin/env bash
#	default color: 178984
oldglyph=#3f2d28
newglyph=#3c2e19

#	Front
#	default color: 36d7b7
oldfront=#78554c
newfront=#785b32

#	Back
#	default color: 1ba39c
oldback=#523a34
newback=#523f22

sed -i "s/#524954/$oldglyph/g" $1
sed -i "s/#9b8aa0/$oldfront/g" $1
sed -i "s/#716475/$oldback/g" $1
sed -i "s/$oldglyph;/$newglyph;/g" $1
sed -i "s/$oldfront;/$newfront;/g" $1
sed -i "s/$oldback;/$newback;/g" $1
