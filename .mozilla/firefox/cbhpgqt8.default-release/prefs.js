// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.migrationsApplied", 10);
user_pref("app.normandy.startupRolloutPrefs.browser.partnerlink.useAttributionURL", true);
user_pref("app.normandy.startupRolloutPrefs.browser.topsites.experiment.ebay-2020-1", true);
user_pref("app.normandy.startupRolloutPrefs.browser.topsites.useRemoteSetting", true);
user_pref("app.normandy.startupRolloutPrefs.doh-rollout.enabled", true);
user_pref("app.normandy.startupRolloutPrefs.doh-rollout.profileCreationThreshold", "1896163212345");
user_pref("app.normandy.startupRolloutPrefs.doh-rollout.provider-steering.enabled", true);
user_pref("app.normandy.startupRolloutPrefs.doh-rollout.provider-steering.provider-list", "[{ \"name\": \"comcast\", \"canonicalName\": \"doh-discovery.xfinity.com\", \"uri\": \"https://doh.xfinity.com/dns-query\" }]");
user_pref("app.normandy.user_id", "5a3f686d-f20f-47f1-8f0c-73c309d083d7");
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1620082294);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1620081814);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1620082054);
user_pref("app.update.lastUpdateTime.region-update-timer", 1620082534);
user_pref("app.update.lastUpdateTime.rs-experiment-loader-timer", 1620081789);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1620081934);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1620082174);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1620081844);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1620082414);
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "custom");
user_pref("browser.discovery.enabled", false);
user_pref("browser.download.dir", "/home/wick3dr0se/src");
user_pref("browser.download.folderList", 2);
user_pref("browser.download.viewableInternally.typeWasRegistered.svg", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.xml", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1620081783);
user_pref("browser.laterrun.bookkeeping.sessionCount", 2);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 107);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.impressionId", "{c59dacca-2ee4-45ba-9108-a3ed01485b87}");
user_pref("browser.newtabpage.activity-stream.showSearch", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.pinned", "[]");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"_1018e4d6-728f-4b20-ad56-37578a4de76b_\"],\"idsInUrlbar\":[\"pocket\",\"_1018e4d6-728f-4b20-ad56-37578a4de76b_\",\"bookmark\"],\"version\":1}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.region.update.updated", 1620081786);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1620083605087");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1620085423087");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1620081793215");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1620103393215");
user_pref("browser.search.region", "US");
user_pref("browser.search.suggest.enabled.private", true);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20210419141944");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1620082664");
user_pref("browser.startup.homepage", "gitlab.com/wick3dr0se");
user_pref("browser.startup.homepage_override.buildID", "20210419141944");
user_pref("browser.startup.homepage_override.mstone", "88.0");
user_pref("browser.startup.lastColdStartupCheck", 1620082663);
user_pref("browser.tabs.drawInTitlebar", false);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"urlbar-container\",\"downloads-button\",\"ublock0_raymondhill_net-browser-action\",\"_73a6fe31-595d-460b-a920-fcc0f8843232_-browser-action\",\"https-everywhere_eff_org-browser-action\",\"jid1-bofifl9vbdl2zq_jetpack-browser-action\",\"jid1-zadieub7xozojw_jetpack-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"import-button\",\"personal-bookmarks\"]},\"seen\":[\"developer-button\",\"ublock0_raymondhill_net-browser-action\",\"_73a6fe31-595d-460b-a920-fcc0f8843232_-browser-action\",\"jid1-zadieub7xozojw_jetpack-browser-action\",\"jid1-bofifl9vbdl2zq_jetpack-browser-action\",\"https-everywhere_eff_org-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":16,\"newElementCount\":3}");
user_pref("browser.uidensity", 1);
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("browser.urlbar.suggest.engines", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1620082673623");
user_pref("devtools.debugger.prefs-schema-version", 11);
user_pref("devtools.everOpened", true);
user_pref("devtools.netmonitor.msg.visibleColumns", "[\"data\",\"time\"]");
user_pref("devtools.toolsidebar-height.inspector", 350);
user_pref("devtools.toolsidebar-width.inspector", 700);
user_pref("devtools.toolsidebar-width.inspector.splitsidebar", 350);
user_pref("distribution.archlinux.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "88.0");
user_pref("distribution.iniFile.exists.value", true);
user_pref("doh-rollout.balrog-migration-done", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("doh-rollout.doorhanger-decision", "UIOk");
user_pref("doh-rollout.mode", 2);
user_pref("doh-rollout.self-enabled", true);
user_pref("dom.forms.autocomplete.formautofill", true);
user_pref("dom.push.userAgentID", "50d8e982250c477b9c6b1616caca6fa7");
user_pref("dom.security.https_only_mode_ever_enabled_pbm", true);
user_pref("dom.security.https_only_mode_pbm", true);
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 33);
user_pref("extensions.getAddons.cache.lastUpdate", 1620082294);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.incognito.migrated", true);
user_pref("extensions.lastAppBuildId", "20210419141944");
user_pref("extensions.lastAppVersion", "88.0");
user_pref("extensions.lastPlatformVersion", "88.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.extension.hidden", false);
user_pref("extensions.ui.lastCategory", "addons://list/extension");
user_pref("extensions.ui.locale.hidden", true);
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.https-everywhere@eff.org", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.jid1-BoFifL9Vbdl2zQ@jetpack", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.jid1-ZAdIEUB7XOzOJw@jetpack", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.{1018e4d6-728f-4b20-ad56-37578a4de76b}", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.{73a6fe31-595d-460b-a920-fcc0f8843232}", true);
user_pref("extensions.webextensions.uuids", "{\"doh-rollout@mozilla.org\":\"e0e3e3bb-3e4d-46a3-b9fa-44d38cd2b038\",\"formautofill@mozilla.org\":\"2d03055b-ded7-42d6-932a-f1e07073f8c1\",\"pictureinpicture@mozilla.org\":\"ead321f5-dfbf-4c00-b253-fbe73f101982\",\"screenshots@mozilla.org\":\"c85429d3-8884-4b06-8fd5-06baca78148b\",\"webcompat-reporter@mozilla.org\":\"2fd9f2a6-d91c-4fd9-b1ac-0d69042393a3\",\"webcompat@mozilla.org\":\"32103436-830c-42e3-a3f3-a1f8850bfa70\",\"default-theme@mozilla.org\":\"2c4c9e46-b839-4a9d-b860-c5204c522226\",\"google@search.mozilla.org\":\"a106538e-e7fe-4f28-a297-ddaf2fe3d945\",\"amazondotcom@search.mozilla.org\":\"7d5f365d-523d-4ee6-a342-ca2aff9d85f6\",\"wikipedia@search.mozilla.org\":\"72b41ee3-3d69-4a88-a621-e34c1a209bae\",\"bing@search.mozilla.org\":\"e38f3fd8-d577-4230-be5d-bd9d46245622\",\"ddg@search.mozilla.org\":\"177ad2a5-95b5-4d54-9352-958359dce5ad\",\"ebay@search.mozilla.org\":\"c9ae294c-4d7f-4fe3-9d9c-d7d15c10d723\",\"uBlock0@raymondhill.net\":\"35b1aebb-0cdd-484a-8bd6-c8b89c1ff67b\",\"{73a6fe31-595d-460b-a920-fcc0f8843232}\":\"4c02d8b2-0042-415f-8e97-79269c92f9cd\",\"jid1-ZAdIEUB7XOzOJw@jetpack\":\"7fa036dd-178b-43da-934a-573259ba8aea\",\"jid1-BoFifL9Vbdl2zQ@jetpack\":\"0d26fba4-8879-4a32-93e5-17f2642ac316\",\"{1018e4d6-728f-4b20-ad56-37578a4de76b}\":\"a62070a5-0603-4a3e-978a-aef8b3b0d70d\",\"https-everywhere@eff.org\":\"27e2ada8-2301-49ff-bd48-3ad673514442\"}");
user_pref("fission.experiment.max-origins.last-disqualified", 0);
user_pref("fission.experiment.max-origins.last-qualified", 1620082030);
user_pref("fission.experiment.max-origins.qualified", true);
user_pref("font.name.serif.x-western", "Source Code Pro");
user_pref("font.size.variable.x-western", 15);
user_pref("layout.spellcheckDefault", 0);
user_pref("media.autoplay.default", 5);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1620082295);
user_pref("media.gmp-gmpopenh264.version", "1.8.1.1");
user_pref("media.gmp-manager.buildID", "20210419141944");
user_pref("media.gmp-manager.lastCheck", 1620082497);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("media.peerconnection.ice.default_address_only", true);
user_pref("media.peerconnection.ice.no_host", true);
user_pref("media.peerconnection.ice.proxy_only_if_behind_proxy", true);
user_pref("network.cookie.cookieBehavior", 1);
user_pref("network.cookie.lifetimePolicy", 2);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.enabled", false);
user_pref("network.prefetch-next", false);
user_pref("network.trr.blocklist_cleanup_done", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("permissions.default.camera", 2);
user_pref("permissions.default.geo", 2);
user_pref("permissions.default.microphone", 2);
user_pref("permissions.default.xr", 2);
user_pref("pref.privacy.disable_button.cookie_exceptions", false);
user_pref("privacy.annotate_channels.strict_list.enabled", true);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);
user_pref("security.remote_settings.crlite_filters.checked", 1620082497);
user_pref("security.remote_settings.intermediates.checked", 1620082497);
user_pref("security.sandbox.content.tempDirSuffix", "d41fbfc2-d6b6-4d5c-b2a6-62c3c758cfe2");
user_pref("security.sandbox.plugin.tempDirSuffix", "4ffb83db-e5b9-48fe-a050-b5d4ba88b0b9");
user_pref("services.blocklist.addons-mlbf.checked", 1620082497);
user_pref("services.blocklist.gfx.checked", 1620082497);
user_pref("services.blocklist.pinning.checked", 1620082497);
user_pref("services.blocklist.plugins.checked", 1620082497);
user_pref("services.settings.clock_skew_seconds", -1);
user_pref("services.settings.last_etag", "\"1620071231814\"");
user_pref("services.settings.last_update_seconds", 1620082497);
user_pref("services.settings.main.anti-tracking-url-decoration.last_check", 1620082497);
user_pref("services.settings.main.cfr-fxa.last_check", 1620082497);
user_pref("services.settings.main.cfr.last_check", 1620082497);
user_pref("services.settings.main.fxmonitor-breaches.last_check", 1620082497);
user_pref("services.settings.main.hijack-blocklists.last_check", 1620082497);
user_pref("services.settings.main.language-dictionaries.last_check", 1620082497);
user_pref("services.settings.main.message-groups.last_check", 1620082497);
user_pref("services.settings.main.nimbus-desktop-experiments.last_check", 1620082497);
user_pref("services.settings.main.normandy-recipes-capabilities.last_check", 1620082497);
user_pref("services.settings.main.partitioning-exempt-urls.last_check", 1620082497);
user_pref("services.settings.main.password-recipes.last_check", 1620082497);
user_pref("services.settings.main.pioneer-study-addons-v1.last_check", 1620082497);
user_pref("services.settings.main.public-suffix-list.last_check", 1620082497);
user_pref("services.settings.main.search-config.last_check", 1620082497);
user_pref("services.settings.main.search-default-override-allowlist.last_check", 1620082497);
user_pref("services.settings.main.search-telemetry.last_check", 1620082497);
user_pref("services.settings.main.sites-classification.last_check", 1620082497);
user_pref("services.settings.main.tippytop.last_check", 1620082497);
user_pref("services.settings.main.top-sites.last_check", 1620082497);
user_pref("services.settings.main.url-classifier-skip-urls.last_check", 1620082497);
user_pref("services.settings.main.websites-with-shared-credential-backends.last_check", 1620082497);
user_pref("services.settings.main.whats-new-panel.last_check", 1620082497);
user_pref("services.settings.security.onecrl.checked", 1620082497);
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.engine.addresses.available", true);
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.tabs.lastSync", "0");
user_pref("toolkit.startup.last_success", 1620082661);
user_pref("toolkit.telemetry.cachedClientID", "c0ffeec0-ffee-c0ff-eec0-ffeec0ffeec0");
user_pref("toolkit.telemetry.pioneer-new-studies-available", true);
user_pref("toolkit.telemetry.previousBuildID", "20210419141944");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
