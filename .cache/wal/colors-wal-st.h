const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#021011", /* black   */
  [1] = "#445A59", /* red     */
  [2] = "#52696A", /* green   */
  [3] = "#5A6E6D", /* yellow  */
  [4] = "#97733F", /* blue    */
  [5] = "#E6A65B", /* magenta */
  [6] = "#DFB36A", /* cyan    */
  [7] = "#e0d5b9", /* white   */

  /* 8 bright colors */
  [8]  = "#9c9581",  /* black   */
  [9]  = "#445A59",  /* red     */
  [10] = "#52696A", /* green   */
  [11] = "#5A6E6D", /* yellow  */
  [12] = "#97733F", /* blue    */
  [13] = "#E6A65B", /* magenta */
  [14] = "#DFB36A", /* cyan    */
  [15] = "#e0d5b9", /* white   */

  /* special colors */
  [256] = "#021011", /* background */
  [257] = "#e0d5b9", /* foreground */
  [258] = "#e0d5b9",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
