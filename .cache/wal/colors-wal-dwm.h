static const char norm_fg[] = "#e0d5b9";
static const char norm_bg[] = "#021011";
static const char norm_border[] = "#9c9581";

static const char sel_fg[] = "#e0d5b9";
static const char sel_bg[] = "#52696A";
static const char sel_border[] = "#e0d5b9";

static const char urg_fg[] = "#e0d5b9";
static const char urg_bg[] = "#445A59";
static const char urg_border[] = "#445A59";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
