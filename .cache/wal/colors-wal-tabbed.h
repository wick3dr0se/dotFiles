static const char* selbgcolor   = "#021011";
static const char* selfgcolor   = "#e0d5b9";
static const char* normbgcolor  = "#52696A";
static const char* normfgcolor  = "#e0d5b9";
static const char* urgbgcolor   = "#445A59";
static const char* urgfgcolor   = "#e0d5b9";
