static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#e0d5b9", "#021011" },
	[SchemeSel] = { "#e0d5b9", "#445A59" },
	[SchemeOut] = { "#e0d5b9", "#DFB36A" },
};
