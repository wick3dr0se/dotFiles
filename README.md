# Arch Linux dotfiles + packages
## My personal dotfiles and packages for Arch Linux using i3wm. This is a bare minimum package installation for a beautiful pre-configured i3 enviornment with no extra bloat. This is my go-to resource for installing my system when I do fresh Arch Linux installations 
#### Desktop enviornment: i3wm
#### Shell: fish
#### Terminal: xterm, no .xresources necessary
#### Theme: gtk, auto-generated w/ pywal & wpgtk based on wallpapers
## How to use 
### Clone repository
> git clone https://gitlab.com/wick3dr0se/dotFiles
### Copy every dotfile into your home directory
> cd dotFiles && cp -r .* ~/
### Install packages from included packages file using yay (https://github.com/Jguer/yay)
##### Edit packages file and exclude any packages that aren't necessary for your system
> yay -S --needed --color=auto $(awk '{print $1}' packages)
